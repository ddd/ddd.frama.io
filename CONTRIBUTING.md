# Contributing

Hey, welcome to the party! 🎉

## Requirements

- PHP 7.1 or above

## Installation

    sudo apt install composer php-mbstring fortunes git
    git clone git@framagit.org:ddd/ddd.git
    cd ddd/symfony
    composer install

## Try it locally

Symfony come with an all-purpose console command in `bin/console` that allows
you, among others, to run a local instance, recreate the database, or recompile
the assets.

For example To start the DDD web server, type:

    bin/console server:start

Then go to `http://localhost:8000` to access to the DDD interface.

## Workflow

We use Story-Driven Development.
You can run the test suite using Behat :

    vendor/bin/behat

The workflow is usually as follows :

1. Edit or create a feature or a scenario.
2. Run `vendor/bin/behat`.
3. If step 2 fails, write some code, then go to 2.
4. Enjoy a cookie!

### Found an error, want a stacktrace ?

Just run behat in verbose mode :

    vendor/bin/behat -vv

## Code Guidelines

Anything goes.