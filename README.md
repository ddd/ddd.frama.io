 
# Democracy-Driven Development Platform

*Because coders are unwitting dictators*


## *Libre software* is a lie

— hey check this out, this project is very good! And it's libre software!

— what is libre software?

— hmmm… it's a software who doesn't own users, but is owned *by* users.

— how?

— because everyone can use it, read the code, modify it, and share their own
versions!

— but I can't read code…

— well, *hem*, indeed… But you can still submit issues on our development
platform.

— Ok, let's see… Hmm sorry but I really don't understand how it works. What is
a commit, a merge request?

— Yes, it's a code development platform actually.

— Hmm. And this is what you call issues, right? Why does this one look
abandoned? It seems quick to do and supported by many coders.

— Oh yes I know. The fact is that, I think, the owner doesn't really like this
one.

— Ok. So your project is written in a language that most users can't
understand, developed with complex tools that most users don't master, and
those who do, decide by themselves what to do, and somewhere, someone decides
alone what to add or not in the software, and this is what you call a libre
software?


## And we aim to fix that

DDDP (Democracy-Driven Development Platform) is a project allowing users to
really take part in the project and clearly understand how it works.

Core principles:

- *inclusivity*: everyone should understand how the software behaves;
- *accessibility*: everyone should be able to alter such behaviors;
- *openness*: everyone should see how the project governance works;
- *benevolence*: let's try to keep greed out of this;
- we [eat our own dog food](https://en.wikipedia.org/wiki/Eating_your_own_dog_food)
: DDDP is developed using DDDP.


## Bring liquid democracy in your software development

*Liquid what?*

> Liquid Democracy, a subset of Delegative Democracy, is a powerful voting model
for collective decision making in large communities. Liquid Democracy combines
the advantages of Direct Democracy and Representative Democracy and creates a
truly democratic voting system that empowers voters to either vote on issues
directly, or to delegate ones voting power to a trusted party.
> 
> Through delegation, people with domain-specific knowledge are able to better
influence the outcome of decisions, which in turn leads to an overall better
governance of the state. Because of this, Liquid Democracy naturally evolves
into a Meritocracy, where decisions are mainly made by those who have the kind
of knowledge and experience required to make well-informed decisions on issues.

— Introduction of [Liquid Democracy: True Democracy for the 21st Century](https://medium.com/organizer-sandbox/liquid-democracy-true-democracy-for-the-21st-century-7c66f5e53b6f).

![](assets/images/liquid_democracy.png)


## How it works

✨ Project owners subscribe their software to DDDP, then link it to its code 
devolpment platform, like Github or Gitlab.

👤 As a DDD software user, (wanting to be part of the development decisions),
you create an account to DDDP and follow the project.

💡 Through the DDDP interface, users can suggest several ideas for the project.

💬 A discussion is then opened with other users and developers.

✍ With the help of these lasts, the specifications of this idea are collectively
composed.

🥒 Specifications are written using [Gherkin syntax](https://docs.cucumber.io/gherkin/reference/),
allowing people to write specifications with natural language. It looks like
that:

```gherkin
Feature: Browsing the features
  In order to understand the specifications of a project
  As a community member
  I want to be able to browse its features in natural language

Scenario: Listing the features
  Given we observe the features of this very project
   When I go on the home page of this project
   Then I should see the feature titled "Browsing the features"
      # Such meta, very wow
```

💧 According to the liquid democracy principles, as long as you have an account
on DDDP, you receive a fixed amount of voting tokens every day.

🗳️ You can use them to vote for a specification that you would like to see,
or delegate them to trusted contributors.

⚖️ Some custom  democracy rules can be specified by project owners
in a configuration file conveniently named `constitution.yml`.

🔎 The constitution, the votes, the specifications and statistics of which one
is implemented, are openness and clearly accessible through DDDP.

⌨️ Coders can now write code and verify it against the needed specifications with
their favorite Gherkin testing tool, like [Behat](http://behat.org/en/latest/)
or [Cucumber](https://docs.cucumber.io/).

💰 DDDP can also be linked to a funding platform, in order to pay contributors
according to the specification they implement.
