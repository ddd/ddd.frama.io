Feature: Browsing the features
  In order to understand the specifications of a project
  As a community member
  I want to be able to browse its features in natural language


@api
Scenario: Listing the features
  Given I observe the features of this very project
   When I request a list of the features
   Then the response should include :
        """
        - title: Browsing the features
        """


@api
Scenario: Listing the features of another project using HTTPS
  Given I observe the features of "https://github.com/Give2Peer/g2p-server-symfony.git"
   When I request a list of the features
   Then the response should include :
        """
        - title: Fixing Issues
        - title: Gaining karma
        """


@api @ssh
Scenario: Listing the features of another project using SSH
  Given I observe the features of "git@github.com:Give2Peer/g2p-server-symfony.git"
   When I request a list of the features
   Then the response should include :
        """
        - title: Fixing Issues
        - title: Gaining karma
        """


# The second listing should not clone again, but use already present files.
@api
Scenario: Listing the features of a project twice
  Given I observe the features of "https://github.com/Give2Peer/g2p-server-symfony.git"
   When I request a list of the features
    And I dump the response
    And I request a list of the features
    And I dump the response
   Then the response should include :
        """
        - title: Fixing Issues
        - title: Gaining karma
        """


@api @noci
Scenario: Listing the features of a big project
  Given I observe the features of "https://github.com/DemocracyEarth/sovereign.git"
   When I request a list of the features
    And I dump the response
   Then the response should include :
        """
        - title: Authoring ideas
        - title: Exposing issues
        """


@api
Scenario: Failing to list the features when target git repository is not provided
   When I try to request a list of features without providing a repository
   Then my request should have been denied


#@ui
#Scenario: Listing the features
#   When I go on the home page of this very project
#   Then I should see the feature titled "Browsing the features"
#      # Such meta, very wow

