
This is the root directory of the Symfony implementation of Gaspacho.


## Install

    composer install


## Configure

    cp .env.dist .env

You need to configure a SSH keypair in `.env`.
The public part of the key should be given to git**b instances if you want to be able to use SSH repository URIs.


## Run the suite

    cp behat.yml.dist behat.yml
    vendor/bin/behat --tags=~ssh