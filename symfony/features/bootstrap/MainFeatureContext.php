<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Testwork\Hook\Scope\AfterSuiteScope;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * This context class contains the definitions of the steps used by the feature files.
 *
 * The organization and refactoring of contexts, step defs and tools is an open subject.
 * Do not hesitate to experiment, rename, refactor, document.
 *
 * Learn how to get started with Behat and BDD on Behat's website.
 * @see http://behat.org/en/latest/quick_start.html
 */
class MainFeatureContext extends BaseFeatureContext
                         implements Context
{
//    use KernelAwareSugar;
//    use RequestAwareTrait;

    /**
     * Symfony's kernel, to which we can send requests and use in assertions about the state of the database and such..
     * @var KernelInterface
     */
    protected $kernel;


    /**
     * The kernel is graciously provided by the bridge between Behat and Symfony.
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }


    // HOOKS ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Prepare the environment, usually set up a clean slate,
     *
     * This is run before each new Scenario.
     *
     * @BeforeScenario
     * @param BeforeScenarioScope $scope
     */
    public function prepare(BeforeScenarioScope $scope)
    {
        $fs = new Symfony\Component\Filesystem\Filesystem();
        $fs->remove($this->getParameter('kernel.cache_dir').DIRECTORY_SEPARATOR.'clones');
    }

    // CONTEXT VARIABLES ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Per scenario, usually "I". We do not use this yet. We will.
     * @var User $user
     */
    protected $i;

    /**
     * Get the user described as "I" in the steps, if one was defined.
     * Note: grabs a "fresh" copy of the user from the database.
     *
     * @return User
     */
    protected function getI()
    {
        if (empty($this->i)) {
            $this->fail("There is no I. Define yourself first, with a step such as :\n".
                        "Given I am a user named \"Tester\"");
        }

        // We want a fresh user from database, because we might have made some
        // requests with this user between its creation and now; Karma !
        return $this->getUser($this->i->getUsername());
    }

    /**
     * Get the user from its username, or null when not foumake it nd.
     *
     * @param $username
     * @return null|User
     */
    protected function getUser($username)
    {
        return $this->getEntityManager()
            ->getRepository("GaspachoBundle:User")
            ->findOneBy(['username'=>$username]);
    }

    // HOOKS ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * To train our inner pigeon into enjoying Feature-Driven Development…
     *
     * @param AfterSuiteScope $scope
     * @AfterSuite
     */
    public static function gimmeCookieNomNomNom(AfterSuiteScope $scope)
    {
        // meme suggestion : a fortune cookie each time the suite runs okay
        if ($scope->getTestResult()->isPassed()) {
            try { print(shell_exec('fortune -a')); } catch (\Exception $e) {}
        }
    }


    // TRANSFORMERS ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * A very handy transformer for integers, registered to Behat.
     *
     * This could be improved to add support for more idiomatic numbers like
     * "one", "a", "forty-two". Not easy to match with a regex !?
     *
     * @Transform /^(-?\d+)$/
     */
    public function castStringToInt($string) { return intval($string); }

    /**
     * A very handy transformer for floats, registered to Behat.
     * @Transform /^(-?\d+\.\d*)$/
     */
    public function castStringToFloat($string) { return floatval($string); }


    // DUMMY STEPS /////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @Given I do nothing
     */
    public function iDoNothing() {}

    /**
     * @Then nothing happens
     */
    public function nothingHappens() {}

    /**
     * @Then I blaze through darkness and light alike
     */
    public function iBlazeThroughDarknessAndLightAlike() {}

    /**
     * @Then it should not raise an exceptmake it ion
     */
    public function itShouldNotRaiseAnException() {}


    // DEBUG STEPS /////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @Given I print :arg1
     */
    public function iPrint($arg1) { print($arg1); }

//    /**
//     * Useful for quick'n dirty debugging.
//     * @Then /^I (?:print|dump) the response$/
//     */
//    public function iDumpTheResponse()
//    {
//        if (empty($this->client)) {
//            $this->fail("No client. Request something first.");
//        }
//        $response = $this->client->getResponse();
//        $content = $response->getContent();
//        try {
//            $content = json_encode(json_decode($content), JSON_PRETTY_PRINT);
//        } catch (\Exception $e) {}
//
//        print($response->getStatusCode() . "\n" . $content . "\n");
//    }

    /**
     * Useful for quick'n dirty debugging.
     * @Then /^I (?:print|dump) myself$/
     */
    public function iDumpMyself()
    {
        if (empty($this->i)) {
            $this->fail("No I. Be someone first.");
        }

        try {
            print(json_encode($this->i, JSON_PRETTY_PRINT));
        } catch (\Exception $e) {
            $this->fail("Nope.");
        }
    }


    // REPOSITORIES ////////////////////////////////////////////////////////////////////////////////////////////////////

    protected $observed_repository;

    /**
     * @Given I observe the features of this very project
     */
    public function iObserveTheFeaturesOfThisVeryProject()
    {
//        $this->observeFeaturesOf("git@framagit.org:ddd/ddd.git");
        $this->observeFeaturesOf("https://framagit.org/ddd/ddd.git");
    }

    /**
     * @Given I observe the features of :repo
     */
    public function iObserveTheFeaturesOfRepo($repo)
    {
        $this->observeFeaturesOf($repo);
    }

    public function observeFeaturesOf($repository)
    {
        $this->observed_repository = $repository;
    }


    // REQUEST STEPS ///////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @When I request a list of the features
     */
    public function iRequestAListOfTheFeatures()
    {
        if (empty($this->observed_repository)) {
            $this->fail("Observe a repository first.");
        }
        $this->requestApi('GET', '/features.json', ['repository' => $this->observed_repository]);
    }

    /**
     * @When I try to request a list of features without providing a repository
     */
    public function iTryToRequestAListOfTheFeaturesNoRepository()
    {
        $this->requestApi('GET', '/features.json', []);
    }


    // RESPONSE STEPS //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @Then /^(?:the|my) request (?:should (?:be|have been)|was) (accepted|denied)$/
     */
    public function theRequestShouldBeAcceptedOrDenied($which)
    {
        switch ($which) {
            case 'accepted':
                $this->assertRequestSuccess();
                break;
            case 'denied':
                $this->assertRequestFailure();
                break;
            default:
                $this->fail("Élu, aimé, jeté, ô poète ! Je miaule !");
        }
    }

    /**
     * Provide YAML in the pystring, it will be arrayed and compared with the
     * other array in the response's data.
     *
     * @Then /^the response should((?: not)?) contain "(.+)"$/
     */
    public function theResponseShouldContain($not='', $that='')
    {
        if (empty($this->client)) {
            throw new Exception("No client. Request something first.");
        }

        $response = $this->client->getResponse();
        $actual = $response->getContent();

        $found = (false !== mb_strpos($actual, $that));

        if (empty($not) && !$found) {
            $this->fail(sprintf(
                "The response did not include the following:\n%s\n" .
                "Because the response provided:\n%s",
                print_r($that, true),
                print_r($actual, true)
            ));
        }

        if (!empty($not) && $found) {
            $this->fail(sprintf(
                "The response actually included the following:\n%s\n" .
                "Because the response provided:\n%s",
                print_r($that, true),
                print_r($actual, true)
            ));
        }
    }

    /**
     * Provide YAML in the pystring, it will be arrayed and compared with the
     * other array in the response's data.
     *
     * @Then /^the response should((?: not)?) include *:?$/
     */
    public function theResponseShouldInclude($not='', $pystring='')
    {
        if (empty($this->client)) {
            throw new Exception("No client. Request something first.");
        }

        $expected = $this->fromYaml($pystring);

        $response = $this->client->getResponse();
        $actual = json_decode($response->getContent(), true);

        $missing = array_diff_assoc_recursive($expected, $actual);
        $notMissing = array_diff_assoc_recursive($expected, $missing);

        if (empty($not) && !empty($missing)) {
            $this->fail(sprintf(
                "The response did not include the following:\n%s\n" .
                "Because the response provided:\n%s",
                print_r($missing, true),
                print_r($actual, true)
            ));
        }

        if (!empty($not) && !empty($notMissing)) {
            $this->fail(sprintf(
                "The response did include the following:\n%s\n" .
                "Because the response provided:\n%s",
                print_r($notMissing, true),
                print_r($actual, true)
            ));
        }
    }

    /**
     * @Then I dump the response
     */
    public function iDumpTheResponse()
    {
        print($this->getStringResponse());
    }



}
