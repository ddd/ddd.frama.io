<?php


trait RequestAwareTrait {

    static $version = 1;

    /**
     * @param array $options
     * @param array $server
     * @return Symfony\Bundle\FrameworkBundle\Client
     */
    protected function getOrCreateClient(array $options = array(),
                                         array $server = array()) {
        if (empty($this->client)) {
            $this->client = $this->createClient($options, $server);
        }
        return $this->client;
    }

    /**
     * Like Client's request, but with our contextual HTTP auth in the headers.
     *
     * @param $method
     * @param $uri
     * @param array $parameters
     * @param array $files
     * @param array $server
     * @param null $content
     * @param bool $changeHistory
     * @return Crawler
     */
    protected function request($method, $uri, array $parameters = array(),
                               array $files = array(), array $server = array(),
                               $content = null, $changeHistory = true)
    {
        if (0 !== strpos($uri, '/')) {
            $uri = '/' . $uri;
        }
        $uri = '/v' . self::$version . $uri;

        $this->client = $this->getOrCreateClient();

        if ( ! empty($this->user)) {

            if (null != $this->password) {
                $password = $this->password;
            } else {
                $password = $this->user->getUsername();
            }

            $server['PHP_AUTH_USER'] = $this->user->getUsername();
            $server['PHP_AUTH_PW']   = $password;
        }

        // Set the desired output format, aka content-type
        // Not actually used by server it seems... Document!?
        //$server['CONTENT_TYPE'] = "application/json";
        // The server understands _format instead.
        // The server ignores that directprivateive and returns json, unless there is
        // and error in which case Symfony kicks in and the error response is
        // returned in the specified _format.
        $parameters['_format'] = 'txt'; // for readable error responses

        $this->crawler = $this->client->request(
            $method, $uri, $parameters, $files,
            $server, $content, $changeHistory
        );

        return $this->crawler;
    }

    public function getStringResponse() {
        return $this->client->getResponse()->getContent();
    }

    public function getJsonResponse() {
        return json_decode($this->getStringResponse());
    }


    /**
     * Asserts that the previously made request was a success.
     * @throws Exception
     */
    public function assertRequestSuccess()
    {
        if (empty($this->client)) {
            throw new Exception("No client. Request something first.");
        }

        $content = $this->client->getResponse()->getContent();
        // Good try, but it floods the console too much :(
        //try {
        //    $content = json_encode(json_decode($content), JSON_PRETTY_PRINT);
        //} catch (\Exception $e) {}

        if ( ! $this->client->getResponse()->isSuccessful()) {
            $this->fail(
                sprintf("Response is unsuccessful, with '%d' HTTP status code ".
                    "and the following content:\n%s",
                    $this->client->getResponse()->getStatusCode(),
                    $content));
        }
    }

    public function assertRequestFailure()
    {
        if (empty($this->client)) {
            throw new Exception("No client. Request something first.");
        }

        $content = $this->client->getResponse()->getContent();
        // Good try, but it floods the console too much :(
        //try {
        //    $content = json_encode(json_decode($content), JSON_PRETTY_PRINT);
        //} catch (\Exception $e) {}

        if ($this->client->getResponse()->isSuccessful()) {
            $this->fail(
                sprintf("Response is successful, with '%d' HTTP status code ".
                    "and the following content:\n%s",
                    $this->client->getResponse()->getStatusCode(),
                    $content));
        }
    }

}