<?php

use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Yaml\Yaml;


/**
 * Returns whatever is in $array1 but not in $array2.
 * Can be optimized, if it matters to you :3
 *
 * Also, should be stored elsewhere, like `extra_functions.php` and loaded via
 * `composer`. We need such a file to import top-level functions that could be
 * part of PHP itself anyways, and it's probably not such a big overhead.
 *
 * @param $array1
 * @param $array2
 * @return array
 */
function array_diff_assoc_recursive($array1, $array2) {
    $diff = array();
    foreach ($array1 as $k => $v) {
        if (!isset($array2[$k])) {
            $diff[$k] = $v;
        }
        else if (!is_array($v) && is_array($array2[$k])) {
            $diff[$k] = $v;
        }
        else if (is_array($v) && !is_array($array2[$k])) {
            $diff[$k] = $v;
        }
        else if (is_array($v) && is_array($array2[$k])) {
            $array3 = array_diff_assoc_recursive($v, $array2[$k]);
            if (!empty($array3)) $diff[$k] = $array3;
        }
        else if ((string)$v != (string)$array2[$k]) {
            $diff[$k] = $v;
        }
    }
    return $diff;
}





/**
 * A base context class for our feature context classes.
 */
abstract class BaseFeatureContext extends Assert
{

    /**
     * Get entity manager.
     *
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->get('doctrine')->getManager();
    }

    /**
     * Get user manager.
     *
     * @return UserManager
     */
    protected function getUserManager()
    {
        return $this->get('fos_user.user_manager');
    }

    /**
     * Returns Container instance.
     *
     * @return ContainerInterface
     */
    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * Gets a parameter.
     *
     * @param string $name The parameter name
     *
     * @return mixed The parameter value
     *
     * @throws InvalidArgumentException if the parameter is not defined
     */
    protected function getParameter($name)
    {
        return $this->getContainer()->getParameter($name);
    }

    /**
     * Get service by id.
     *
     * @param string $id
     *
     * @return object
     */
    protected function get($id)
    {
        return $this->getContainer()->get($id);
    }

    /**
     * Returns fixed step argument (with \\" replaced back to ").
     *
     * @param string $argument
     *
     * @return string
     */
    protected function fixStepArgument($argument)
    {
        return str_replace('\\"', '"', $argument);
    }

    /**
     * Useful variable transformer, that we choose to use manually.
     * If we can somehow specify what transformer a pystring should be submitted
     * to directly in the gherkin without clogging it or loosing the intuitivity
     * of it, this can become in the future an annotated Transformer.
     *
     * @param $pystring
     * @return array
     */
    protected function fromYaml($pystring)
    {
        return Yaml::parse($pystring, true, true);
    }

    /**
     * Recursively removes the $directory and all its contents.
     * If $directory does not exist, silently ignore when not $strict.
     *
     * @param string $directory
     * @param bool $strict
     * @throws Exception
     */
    protected static function removeDirectory($directory, $strict = true)
    {
        if (is_dir($directory)) {
            foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST) as $path) {
                $path->isDir() && !$path->isLink() ? rmdir($path->getPathname()) : unlink($path->getPathname());
            }
            rmdir($directory);
        } else {
            if ($strict) {
                throw new Exception("Directory '$directory' does not exist.");
            }
        }

//        // todo: try this out
//        $fs = new Symfony\Component\Filesystem\Filesystem();
//        $fs->remove($directory);
    }



    /**
     * Creates a Client.
     *
     * @param array $options An array of options to pass to the createKernel class
     * @param array $server  An array of server parameters
     *
     * @return Client A Client instance
     */
    protected function createClient(array $options = array(), array $server = array())
    {
        $client = $this->kernel->getContainer()->get('test.client');
        $client->setServerParameters($server);

        return $client;
    }



    /** @var string $version The version of the API to use */
    protected $version = '1';

    /** @var Client $client */
    protected $client;

    /** @var Crawler $crawler */
    protected $crawler;


    /**
     * @param array $options
     * @param array $server
     * @return Client
     */
    protected function getOrCreateClient(array $options = array(),
                                         array $server = array()) {
        if (empty($this->client)) {
            $this->client = $this->createClient($options, $server);
        }
        return $this->client;
    }

    /**
     * Like Client's request, but with our contextual HTTP auth in the headers, and the /api/v1 prefix.
     *
     * @param $method
     * @param $uri
     * @param array $parameters
     * @param array $files
     * @param array $server
     * @param null $content
     * @param bool $changeHistory
     * @return Crawler
     */
    protected function requestApi($method, $uri, array $parameters = array(),
                                  array $files = array(), array $server = array(),
                                  $content = null, $changeHistory = true)
    {
        if (0 !== strpos($uri, '/')) {
            $uri = '/' . $uri;
        }
        $uri = '/api/v' . $this->version . $uri;

        $this->client = $this->getOrCreateClient();

        if ( ! empty($this->user)) {

            if (null != $this->password) {
                $password = $this->password;
            } else {
                $password = $this->user->getUsername();
            }

            $server['PHP_AUTH_USER'] = $this->user->getUsername();
            $server['PHP_AUTH_PW']   = $password;
        }

        // Set the desired output format, aka content-type
        // Not actually used by server it seems... Document!?
//        $server['CONTENT_TYPE'] = "application/json";
        $server['CONTENT_TYPE'] = "text/plain";
        // The server understands _format instead.
        // The server ignores that directive and returns json, unless there is
        // an error in which case Symfony kicks in and the error response is
        // returned in the specified _format.
        $parameters['_format'] = 'txt'; // for readable error responses

        $this->crawler = $this->client->request(
            $method, $uri, $parameters, $files,
            $server, $content, $changeHistory
        );

        return $this->crawler;
    }

    public function getStringResponse() {
        return $this->client->getResponse()->getContent();
    }

    public function getJsonResponse() {
        return json_decode($this->getStringResponse());
    }


    /**
     * Asserts that the previously made request was a success.
     * @throws Exception
     */
    public function assertRequestSuccess()
    {
        if (empty($this->client)) {
            throw new Exception("No client. Request something first.");
        }

        $content = $this->client->getResponse()->getContent();
        // Good try, but it floods the console too much :(
        //try {
        //    $content = json_encode(json_decode($content), JSON_PRETTY_PRINT);
        //} catch (\Exception $e) {}

        if ( ! $this->client->getResponse()->isSuccessful()) {
            $this->fail(
                sprintf("Response is unsuccessful, with '%d' HTTP status code ".
                    "and the following content:\n%s",
                    $this->client->getResponse()->getStatusCode(),
                    $content));
        }
    }

    public function assertRequestFailure()
    {
        if (empty($this->client)) {
            throw new Exception("No client. Request something first.");
        }

        $content = $this->client->getResponse()->getContent();
        // Good try, but it floods the console too much :(
        //try {
        //    $content = json_encode(json_decode($content), JSON_PRETTY_PRINT);
        //} catch (\Exception $e) {}

        if ($this->client->getResponse()->isSuccessful()) {
            $this->fail(
                sprintf("Response is successful, with '%d' HTTP status code ".
                    "and the following content:\n%s",
                    $this->client->getResponse()->getStatusCode(),
                    $content));
        }
    }

}