
## Where are the features ?

The `*.feature` files are in the `features` directory at the root of this project (sibling of `symfony`).

You can also write features in this directory, if they are tied to symfony for example, or for debugging.


## Show me the goodies

To set up the feature suite, once :

    cd symfony
    cp behat.yml.dist behat.yml


To run the feature suite :

    cd symfony
    vendor/bin/behat

