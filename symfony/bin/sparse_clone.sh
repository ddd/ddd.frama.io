#!/usr/bin/env bash

# Input arguments
repository=$1
target=$2
# and then directories


if [ -z "$repository" ] || [ -z "$target" ] ; then
    cat <<fin
Clone the repository, but try to retrieve as few data as possible.

Usage :
  sparse_clone.sh repository target [directory, …]
    [repository] : Full URI of the repository. (SSH or HTTPS)
    [target]     : Filename-safe name for the target directory on the filesystem.
fin
    exit
fi

echo "I am $(whoami)."

# Cloning using SSH prompts if the target is not in known_hosts.
known_hosts="$HOME/.ssh/known_hosts"

# Make an empty one if it does not exist, and make dir tree as needed.
if [ ! -f ${known_hosts} ] ; then
    install -Dv /dev/null ${known_hosts}
else
    echo "Using known_hosts at $known_hosts"
fi

# Extract the domain name from the repository URL, be it SSH or HTTPS or SCP-like.
# Probably won't work with local (filesystem) URIs. (there's no domain, or maybe localhost?)
domain=""
domainMatch='^([^@]+?(//|@))?([^/@:]++)[/:]'
[[ ${repository} =~ ${domainMatch} ]] && domain=${BASH_REMATCH[-1]}

if [ -z "$domain" ] ; then
    echo "Could not figure out the domain of repository '$repository'."
    echo "Got matches : ${BASH_REMATCH[@]}"
    echo "Skipping domain addition to known hosts…"
else
    echo "Target domain : ${domain}"
    # Add the target domain to known hosts if not already in it
    ssh-keygen -F ${domain} || ssh-keyscan -t rsa ${domain} >> ${known_hosts}
fi

# Debug
#echo "known_hosts:"
#cat ${known_hosts}
#echo "------------"


# Let's use the ENV vars for the keypair
# Ideally the secret should stay in the env vars and never be written to file.
# If you know how to do that, to make git use the keypair from env, please tell us. :)
# We're removing the files at the end of the script, but… You know.

gaspacho_id_rsa_pri_file="${HOME}/.ssh/gaspacho_id_rsa"
gaspacho_id_rsa_pub_file="${HOME}/.ssh/gaspacho_id_rsa.pub"

echo -e "${GASPACHO_ID_RSA_PRIVATE}" > ${gaspacho_id_rsa_pri_file}
echo -e "${GASPACHO_ID_RSA_PUBLIC}" > ${gaspacho_id_rsa_pub_file}

chmod 600 ${gaspacho_id_rsa_pri_file}
chmod 661 ${gaspacho_id_rsa_pub_file}


# And now, the actual git cloning

function git_sparse_clone() (
  remoteurl="$1" localdir="$2" && shift 2

  mkdir -p "$localdir"
  cd "$localdir"

  # We don't directly clone so that we can set git/info/sparse-checkout first.
  # It would be nice to be able to retrieve master only, using --single-branch,
  # but that option seems to be available on git clone only. -_-
  git init

  # Make sure we configure the SSH KeyPair before we add the remote or pull.
  git config core.sshCommand "ssh -i ${gaspacho_id_rsa_pri_file}"
  git config core.sparseCheckout true

  # Loop over remaining args, the whitelist of directories to check out.
  for i; do
    echo "$i" >> .git/info/sparse-checkout
  done

  # Let's grab the data
  git remote add -f --no-tags -t master origin "$remoteurl"
  git pull --depth=1 --no-tags origin master
)

function git_clone() (
  remoteurl="$1" localdir="$2" && shift 2

  mkdir -p "$localdir"
  cd "$localdir"

  git init
  git remote add -f origin "$remoteurl"

  git pull origin master
)


#git_clone $@
git_sparse_clone $@



# Clean up after ourselves.
rm ${gaspacho_id_rsa_pri_file}
rm ${gaspacho_id_rsa_pub_file}
