<?php

namespace App\Controller\Api;

use App\Repository\FeatureRepository;
use Behat\Gherkin\Keywords\ArrayKeywords;
use Behat\Gherkin\Lexer;
use Behat\Gherkin\Node\FeatureNode;
use Behat\Gherkin\Parser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
// For annotations – it's okay if your IDE can't find their use.
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Feature;


/**
 * Anyone remembers how to set the /api prefix class-wise ?
 *
 * Let's make the first API directly under v1, it's less hassle later.
 *
 * Responsibilities:
 *   - Features CRUD
 *   - …
 */
class ApiV1Controller extends AbstractController
{
    /**
     * @api {GET} /api/v1/features.json api_v1_features_list
     * @apiDescription List the features of the target repository.
     *
     * @apiGroup features
     * @apiParam {string} repository SSH or HTTPS URI of the repository.
     * @apiSuccess {array} A list of features, sorted alphabetically by pathname.
     *
     * List the features of the target repository.
     *
     * @SWG\Parameter(
     *     name="repository",
     *     in="query",
     *     type="string",
     *     description="SSH or HTTPS URI of the repository."
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="The list of features is sorted alphabetically by pathname.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Feature::class, groups={"full"}))
     *     )
     * )
     *
     * @Route("/api/v1/features.json", methods={"GET"}, name="api_v1_features_list")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request) : Response
    {
        // Gets the value of the defaults in routing config (html, but we set txt), before _format :(
//        print($request->getRequestFormat());
        // Actual value we want
//        print($request->query->get('_format'));

        $repository = $request->query->get('repository');
        if (empty($repository)) {
            throw new BadRequestHttpException("No repository provided.");
        }

//        $ft = $this->get("app.repository.features");
        $ft = new FeatureRepository($this->container);
        $features = $ft->getFeaturesOfGitRepository($repository);

        return $this->json($features);
    }


}
