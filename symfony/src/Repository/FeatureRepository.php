<?php

namespace App\Repository;

use App\Entity\Feature;
use Behat\Gherkin\Keywords\ArrayKeywords;
use Behat\Gherkin\Lexer;
use Behat\Gherkin\Parser;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\ServiceLocator;
use Symfony\Component\Finder\Finder;
use Behat\Gherkin\Node\FeatureNode;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class FeatureRepository
{
    /**
     * @var ServiceLocator
     */
    protected $container;

    /**
     * FeatureRepository constructor.
     * @param ServiceLocator $container
     */
    public function __construct(ServiceLocator $container)
    {
        $this->container = $container;
    }

    /**
     * Returns a list of Feature instances for all the features of the target git repository.
     *
     * @param string $git_repository_url
     * @return Feature[]
     */
    public function getFeaturesOfGitRepository(string $git_repository_url) : array
    {
        $features_root_directory = 'features'; // 🐵

        $this->retrieveFilesOfRepositoryIfNeeded($git_repository_url);

        $repository_directory = $this->getGitRepositoryDirectory($git_repository_url);
        $directory = $repository_directory . DIRECTORY_SEPARATOR . $features_root_directory;
        $features_paths = $this->getFeaturesInTree($directory);

        $features = [];
        foreach ($features_paths as $feature_path) {
            $feature_node = $this->parseFeatureNodeFromFile($feature_path);
            $features[] = new Feature($feature_node);
        }

        return $features;
    }

    /**
     * @param String $path Path to the file.
     * @return FeatureNode
     */
    public function parseFeatureNodeFromFile($path)
    {
        $feature_contents = file_get_contents($path);

        // todo: use CucumberKeywords and a (public?) file ?
        $keywords = new ArrayKeywords(array(
            'en' => array(
                'feature'          => 'Feature',
                'background'       => 'Background',
                'scenario'         => 'Scenario',
                'scenario_outline' => 'Scenario Outline|Scenario Template',
                'examples'         => 'Examples|Scenarios',
                'given'            => 'Given',
                'when'             => 'When',
                'then'             => 'Then|So',
                'and'              => 'And',
                'but'              => 'But'
            ),
            'fr' => array(
                'feature'          => 'Fonctionnalité',
                'background'       => 'Contexte|Descriptif',
                'scenario'         => 'Scénario',
                'scenario_outline' => 'Scénario Multiple',
                'examples'         => 'Exemples|Scénarios',
                'given'            => 'Sachant que|Donné que',
                'when'             => 'Quand',
                'then'             => 'Alors',
                'and'              => 'Et',
                'but'              => 'Mais'
            )
        ));

        $lexer  = new Lexer($keywords);
        $parser = new Parser($lexer);
        return $parser->parse($feature_contents, $this->makePathRelativeToCacheOfClones($path));
    }

    public function makePathRelativeToCacheOfClones($absolute_path) : string
    {
        $clones_dir = $this->getOrCreateClonesDirectory();
        if (mb_strpos($absolute_path, $clones_dir) !== 0) {
            throw new \Exception("Path '$absolute_path' is not under cache of clones '$clones_dir'.");
        }

        return mb_substr($absolute_path, mb_strlen($clones_dir)+1);
    }

    /**
     * find -name "*.feature" $root_dir
     * @param string $root_dir
     * @return string[]
     */
    public function getFeaturesInTree($root_dir) : array
    {
        $finder = new Finder();
        $finder->files()
            ->in($root_dir)
            ->name('/[.]feature$/')
            ->sortByName();
        $paths = [];
        foreach ($finder as $ff) {
            $paths[] = $ff->getPathname();
        }

        return $paths;
    }

    /**
     * Retrieves or updates the repository files on our server.
     * Quite barbaric, since this is done on *every* api call.
     *
     * @param $repository
     */
    public function retrieveFilesOfRepositoryIfNeeded($repository)
    {
        $directory = $this->getGitRepositoryDirectory($repository);
        $clones_dir = $this->getOrCreateClonesDirectory();
        $root_dir = $this->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR . '..';

        if ( ! is_dir($directory)) {
            // We have nothing in our cache, let's clone ! … and get DDOSed back into oblivion.
            $name = $this->makeSafeDirectoryName($repository);

            $sparse_clone = $root_dir . DIRECTORY_SEPARATOR . 'bin'
                . DIRECTORY_SEPARATOR . 'sparse_clone.sh';

            $process_clone = new Process(
                [$sparse_clone, $repository, $name, 'features'],
                $clones_dir,
                null, null, null
            );
            $process_clone->run(); // run() hogs execution until the command finishes

            // debug, remove
//            print("\n------------\n");
//            print($process_clone->getOutput());
//            print("\n------------\n");
//            print($process_clone->getErrorOutput());
//            print("\n============\n");

            if ( ! $process_clone->isSuccessful()) {
                throw new ProcessFailedException($process_clone);
            }

        } else {
            // We have a repo already, let's pull !
            // This is quite barbaric -> use CRON, or a simple timestamp-based throttle
            $process = new Process(
                ['git', 'pull', 'origin', 'master'],
                $directory,
                null, null, null  # to disable the timeout
            );
            $process->run();

            if ( ! $process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }
        }
    }

    /**
     * @param string $git_repository_url
     * @return string Absolute path to the directory of the repository.
     */
    public function getGitRepositoryDirectory(string $git_repository_url) : string
    {
        $clones_dir = $this->getOrCreateClonesDirectory();
        $name = $this->makeSafeDirectoryName($git_repository_url);

        return $clones_dir . DIRECTORY_SEPARATOR . $name;
    }

    /**
     * @return string The absolute path to the clones directory in the cache.
     */
    public function getOrCreateClonesDirectory() : string
    {
        $d = $this->getParameter('app.clones.dir');

        if ( ! is_dir($d)) { mkdir($d); }

        return $d;
    }

    /**
     * Generate a unique but safe directory name from a git repository URI.
     * Please, don't use hashes for unique ids. Collisions ! Make your own luck.
     *
     * @param string $git_repository_url
     * @return string
     */
    public function makeSafeDirectoryName(string $git_repository_url) : string
    {
        return base64_encode($git_repository_url);
    }

    /**
     * Gets a container parameter by its name.
     *
     * @return mixed
     */
    protected function getParameter(string $name)
    {
        if (!$this->container->has('parameter_bag')) {
            throw new ServiceNotFoundException('parameter_bag', null, null, array(), sprintf('The "%s::getParameter()" method is missing a parameter bag to work properly. Did you forget to register your controller as a service subscriber? This can be fixed either by using autoconfiguration or by manually wiring a "parameter_bag" in the service locator passed to the controller.', \get_class($this)));
        }

        return $this->container->get('parameter_bag')->get($name);
    }

}