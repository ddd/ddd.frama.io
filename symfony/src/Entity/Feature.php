<?php

namespace App\Entity;

use Behat\Gherkin\Node\FeatureNode;
use Swagger\Annotations as SWG;

class Feature implements \JsonSerializable
{
    /**
     * @SWG\Property(type="string", maxLength=255)
     */
    public $title;

    /**
     * Of note: we pre-processed the file attribute to be relative to the clones directory.
     * @var FeatureNode
     */
    protected $node;

    /**
     * Feature constructor.
     * @param FeatureNode $node
     */
    public function __construct(FeatureNode $node)
    {
        $this->node = $node;
    }

    /**
     * Public values only.
     *
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        $id = base64_encode($this->node->getFile());
        return [
            'id' => $id,
            'title' => $this->node->getTitle(),
//            'file' => $this->node->getFile(),
        ];
    }
}